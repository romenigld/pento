defmodule PentoWeb.WrongLive do
  use PentoWeb, :live_view

  def mount(_params, session, socket) do
    socket =
      assign(
        socket,
        score: 0,
        message: "Guess a number.",
        time: time(),
        random_n: random_number(),
        user: Pento.Accounts.get_user_by_session_token(session["user_token"]),
        session_id: session["live_socket_id"]
      )

    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <h1>Your score: <%= @score %></h1>
    <h2>
      <%= @message %>
      It's <%= @time %>
    </h2>
    <h2>
      <%= for n <- 1..10 do %>
        <a href="#" phx-click="guess" phx-value-number="<%= n %>"><%= n %></a>
      <% end %>
    </h2>
    <pre>
      <%= @user.email %>
      <%= @session_id %>
    </pre>
    """
  end

  def handle_event("guess", %{"number" => guess} = data, socket) do
    IO.inspect data, label: "Data"

    guess = guess |> String.to_integer()

    if (guess === socket.assigns.random_n) do
      socket = assign(socket, :message, "Your guess: #{guess}. Is correct, You win +1 score!")
      socket = assign(socket, :score, socket.assigns.score + 1)

      {:noreply, assign(socket,
                        random_n: random_number(),
                        time: time())}
    else
      socket = assign(socket, :message, "Your guess: #{guess}. Wrong! Guess again.")
      socket = assign(socket, :score, socket.assigns.score - 1)

      {:noreply, assign(socket, time: time())}
    end
  end

  def time do
    DateTime.utc_now
    |> to_string
  end

  def random_number do
    1..10
    |> Enum.random()
  end
end
